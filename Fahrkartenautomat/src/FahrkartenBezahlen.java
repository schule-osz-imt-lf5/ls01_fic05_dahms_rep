package de.Dahms.Fahrkartenautomat;
import java.util.Scanner;

public class FahrkartenBezahlen {
	Scanner sc = new Scanner(System.in);
	
	public double fahrkartenBezahlen(double cost)
	{
		double money;	
		money = 0.0; 
		
		while (money < cost)
		{
			double price = cost - money;
			double moneyIn;
			System.out.printf("Noch zu zahlen: %.2f", price).print(" Euro \n");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			moneyIn = sc.nextDouble();
			money += moneyIn;
			
		}
		
		return money;
	}

	
	

}
