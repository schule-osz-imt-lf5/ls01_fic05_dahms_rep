package de.Dahms.Fahrkartenautomat;
import java.util.Scanner;

public class FahrkartenbestellungErfassen {
	
	Scanner sc = new Scanner(System.in);
	String[] fahrkarte = {
			"  Einzelfahrscheine [2,90 EUR] (1)",
			"  Tageskarten [8,60 EUR] (2)",
			"  Kleingruppen-Tageskarten [23,50 EUR] (3)"
		};
	double[] preise = {
			2.90,
			8.60,
			23.50
		};
	
	
	public void fahrkartenbestellungErfassen()
	{
		FahrkartenbestellungErfassen use = new FahrkartenbestellungErfassen();
		
		System.out.println("\n\nW�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
		for (String s : fahrkarte)
		{
			System.out.println(s);
		}
		System.out.print("\nIhre Wahl: ");
		use.bestellung();
	}
	
	public void bestellung()
	{
		FahrkartenbestellungErfassen use = new FahrkartenbestellungErfassen();
		FahrkartenBezahlen payOrder = new FahrkartenBezahlen();
		RueckgeldAusgeben returnMoney = new RueckgeldAusgeben();
		
		double 	price = use.auswahl();
		int 	tickets = use.amount();
		double 	cost = cost(tickets, price);
		double 	money = payOrder.fahrkartenBezahlen(cost);
		returnMoney.returnMoney(cost, money);
		
	}
	
	
	public double auswahl()
	{
		
		int choice;
		choice = sc.nextInt();
		double price;

		while (!(choice < 4  && choice > 0)) {
			System.out.println(">>falsche Eingabe<<");
			System.out.print("\nIhre Wahl: ");
			choice = sc.nextInt();

		}
		choice -= 1;

		
		price = preise[choice];
		return price;
	}
	
	public int amount()
	{
		int tickets;
		System.out.print("Anzahl der Tickets: ");
		tickets = sc.nextInt();
		while (tickets > 10 || tickets < 1) 
		{
			System.out.println("Die Anzahl der Tickets ist ung�ltig!");
			System.out.print("Anzahl der Tickets (1-10): ");
			tickets = sc.nextInt();
		} 
		//System.out.printf("Tickets: %d \n", tickets);
		return tickets;
	}
	
	static double cost(int tickets, double price)
	{
		
		return tickets * price;
	}
	

	public void header() {
		// TODO Auto-generated method stub
		String order = "\nFahrkartenbestellvorgang:";
		
		System.out.printf("%s \n", order);
		for (int i = 0; i < order.length(); i++) 
		{
			System.out.print("=");
		}
	}

	

}
