package de.Dahms.Fahrkartenautomat;

public class RueckgeldAusgeben {
	
	static void warte(int ms)
	{
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("="); 
	          try {
				Thread.sleep(ms);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public void returnMoney(double cost, double money)
	{
		warte(200);
		double moneyOut; 
		
		moneyOut = money - cost;
	       if(moneyOut > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO \n", moneyOut);
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	    	   
	           while(moneyOut >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
	        	  moneyOut -= 2.0;
		          // r�ckgabebetrag = r�ckgabebetrag - 2.0;
	           }
	           while(moneyOut >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO"); 
	        	  moneyOut -= 1.0;
	           }
	           while(moneyOut >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
	        	  moneyOut -= 0.5;
	           }
	           while(moneyOut >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	        	  moneyOut -= 0.2;
	           }
	           while(moneyOut >= 0.09) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
	        	  moneyOut -= 0.1;
	           }
	           while(moneyOut >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	        	  moneyOut -= 0.05;
	           }

	       }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir w�nschen Ihnen eine gute Fahrt.\n");
	}
}
