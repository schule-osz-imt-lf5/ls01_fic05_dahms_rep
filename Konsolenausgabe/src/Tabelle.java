
public class Tabelle {

	public static void main(String[] args) {
		
		String a = "Fahrenheit";
		String b = "Celsius";
		String c = "%-12s | %10s \n";
		String d = "%-12d | %10.2f \n";
		String e = "%+-12d | %10.2f \n";
		
		int fa = -20;
		int fb = -10;
		int fc = 0;
		int fd = 20;
		int fe = 30;
		
		double ca = -28.8889;
		double cb = -23.3333;
		double cc = -17.7778;
		double cd = -6.6667;
		double ce = -1.1111;
		
		System.out.printf(c, a, b);
		System.out.println("-------------------------");
		System.out.printf(d, fa, ca);
		System.out.printf(d, fb, cb);
		System.out.printf(e, fc, cc);
		System.out.printf(e, fd, cd);
		System.out.printf(e, fe, ce);
		

	}

}
