
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		System.out.printf("%s", "Fahrenheit | ");
		System.out.printf("%5s", "Celsius");
		System.out.printf("\n%s\n", "--------------------");
		System.out.printf("%s", "-20");
		System.out.printf("%16s", "| -28.89");
		System.out.printf("\n%s", "-10");
		System.out.printf("%16s\n", "| -22.33");
		System.out.printf("%s", "+0");
		System.out.printf("%17s", "| -17.78");
		System.out.printf("\n%s", "+20");
		System.out.printf("%15s", "| -6.67");
		System.out.printf("\n%s", "+30");
		System.out.printf("%15s", "| -1.11");
	}

}
